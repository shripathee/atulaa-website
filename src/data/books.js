const BOOKS = [{
  id: 101,
  title: 'Meditations',
  content: `Human problems and preoccupations have stayed the same for centuries, and minds like Marcus Aurelius' have done the hard work of mindfully grappling with them and given us a template for self-reflection. If you were as struck by the the opening chapter of Meditations (an ode to every person in Aurelius' life and everything they've taught him) as I was, you are sure to have at least a week's worth of journaling exercises to look forward to. What a guy. Even though I read this very slowly, one essay a day, I will be revisiting a more contemporary translation soon.`
}, {
  id: 102,
  title: 'The Testaments',
  content: `Highly satisfying sequel, every bit worth the hype. HT and Testaments fulfil very different purposes - HT was shocking because of the idea that modern society could so easily devolve into Gilead, the sequel because of how little and big acts of complicity sustain an unjust regime. While the first book already gives you closure in revealing that Gilead eventually fell, I'm glad Testaments came out, in my head this is Offred's story.
  I can see how it could be viewed as a fan servicing exercise, but what can I say, as a fan I feel happily serviced.`
}, {
  id: 103,
  title: 'A Gentleman in Moscow',
  content: `This is such an incredible book. I didn't expect to love it based on the blurb - 'man is under house arrest in a hotel in Moscow', how engaging can that be? But Amor Towles characterizes the Count and the incidents of his life so well, finishing the book was like parting with a friend. So many lovely turns of phrase, and many points at which I just wanted to stop and savour the imagery. Will definitely be re-reading.`
}, {
  id: 104,
  title: 'The Pillars of the Earth',
  content: `Hmm where do I begin. Pillars of the Earth is about a very interesting phase in medieval English history now referred to as the Anarchy. If, like me, you know very little about it, the book is a great crash course. It's also the exact kind of historical fiction I'd typically like to read - acts of individuals and how they have huge historical implications. My 3 stars are entirely for this, and the amazing Prior Phillip. This book however overestimates how interesting (and likeable) the other individual characters are, and ignores the far more fascinating political background in the process. The architectural descriptions were dull and far too many, and are probably the reason why the book is as big as it is. This was a kindle read, and I did not know when I started this book that it was nine hundred freakin' pages long. That said, it was a definite page turner, and it kept me going despite my other issues with the book.
  One huge issue I had with the book is that Ken Follett (whom I really enjoyed reading at 16-17) just does NOT know how to write his female characters. Tom Builder at one point wonders if the forest outlaws ever tried rape the beautiful Ellen, and 'his loins stir at the thought, although he had never taken a woman against her will, not even his wife'. Aww, what a nice guy. Man of the year. I'd give this historical license, if the author didn't KEEP describing his 'good men' as people who'd managed to not rape their partners. The sex scenes are far too gratuitous, every time the main female character is mentioned, so are her 'large breasts'... I understand this was written in 1989, but as a reader in 2019, it's hard to keep it from affecting one's reading experience.`
}, {
  id: 105,
  title: 'City of Girls',
  content: `There was enough in this book to keep me reading - Elizabeth Gilbert's easy writing style, the young person in new york city genre (of which I'm a fan) and the hope that the plot would go somewhere. The writing style held up throughout, but my hope that a plot of some kind would materialize was left unfulfilled. It's a book I'd call a chill vacation read, if it had been 300 pages long rather than 600.
  (Spoilers)
  20 year old Vivian Morris isn't as interesting a character as her 90 year old self thinks she is, which makes the first 70% of the book way too detailed for my liking. The central conceit of the book is that it's a letter that explains Vivian's relationship with the recipient's father, which I wholly buy into. But I refuse to believe so much of said letter would talk about ONE year in Vivian's life and not the 15 years that she actually knew the father. I wish Gilbert had put as much thought into developing Vivian's 30s and 40s, she was definitely a more interesting, insightful character in those parts of the book - someone to match the well-written incredible characters from the Lily in the first half.
  I honestly enjoyed the writing so much that none of these issues occurred to me while I was reading, but I was left wondering 'what did I just read..?' by the time I was done. If I were a 70 year old woman hoping to know how Vivian knew my father but instead received this tome in the mail about the glory days of a woman I barely knew, I would just cry.`
}];

export default BOOKS;