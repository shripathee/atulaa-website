import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import AboutMe from './containers/AboutMe'
import Books from './containers/Books';
import Law from './containers/Law';
import DoGood from './containers/DoGood';


Vue.config.productionTip = false

Vue.use(VueRouter);

const routes = [
  { path: '', beforeEnter: (to, from, next) => next('/about-me') },
  { path: '/about-me', component: AboutMe, name: 'about-me' },
  { path: '/books', component: Books, name: 'books' },
  { path: '/law', component: Law, name: 'law' },
  { path: '/do-good', component: DoGood, name: 'do-good' }
];

const router = new VueRouter({
  routes,
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
